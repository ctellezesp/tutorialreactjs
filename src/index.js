import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//import Corgi, {eats} from './corgi'

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

/*
let array = [1,2,3];
array = [3,4,5];
console.log(array);

const person = {
	name: 'Josh',
	tick(){
		console.log(this);
	}
}
person.tick();

const tick = person.tick.bind(person);
tick();

const dog = {
	eat(){
		setTimeout(() =>
			console.log(this), 1000);
	}
}
dog.eat();

let colors = ['blue', 'red', 'green'];
let missing_colors = ['orange'];

let allColors = [...colors, ...missing_colors];
console.log(allColors);

let jingleBells = {
	red: 2,
	blue: 3,
	orange: 5
}
let newJingle = {...jingleBells};
console.log(newJingle);

const {red, blue, orange} = jingleBells;
console.log(red,blue,orange);
//Para copias usar express sintax


//Classes and inheritance

class Person{
	constructor(...args){
		this.name = args[0];
		this.lastName = args[1];
		this.age = args[2];
	}

	talk(){
		console.log(this);
		return true;
	}
}

let person = new Person("Carlos");
//console.log(person);
person.talk();
console.log(person.talk());

class Student extends Person{
	constructor(name, lastName, age, studentId){
		super(name, lastName, age); //primero ejecuta el del padre y luego el mio
		this.studentId = studentId;
	}
}

let john = new Student('Josh', 'Nicols', 18,  'A01274912');
console.log(john);

let corgi = new Corgi('Thor');
console.log(corgi);
eats();
*/

class Square extends React.Component { //clase Square(componente) con herencia de componente react

  render() { //método que define lo que se va a mostrar en la aplicación
    return ( //botón con evento onClick
      <button className="square" onClick={() => this.props.onClick() }> 
        {this.props.value}
      </button>
    );
  }
}

class Board extends React.Component {//clase Board(Componente) con herencia de componente react

	constructor(props) {//constructor sobrecargado con propiedades
    super(props); //primer ejecuta el constructor padre y luego el hijo
    this.state = {//propiedades de estado de la clase
      squares: Array(9).fill(null),
      xIsNext: true,
    };
  }

  renderSquare(i) { //método para generar squares
    return <Square value={this.state.squares[i]} onClick={() => this.handleClick(i)}/>;
  }

  handleClick(i) { //método para determinar el turno al dar clic
    const squares = this.state.squares.slice();
    if(this.calculateWinner(squares) || squares[i]){
    	return;
    }
    squares[i] = this.state.xIsNext? 'X' : 'O'; //if ternario para determinar x u o
    this.setState({ //actualizar el estado de la clase
    	squares: squares,
    	xIsNext: !this.state.xIsNext
    });
  }

  calculateWinner(squares){ //método para determinar casos para ganar
  	const lines = [
  		[0, 1, 2],
	    [3, 4, 5],
	    [6, 7, 8],
	    [0, 3, 6],
	    [1, 4, 7],
	    [2, 5, 8],
	    [0, 4, 8],
	    [2, 4, 6],
  	]

  	for(let i=0; i<lines.length; i++){ //for para determinar si un jugador ha ganado
  		const [a,b,c] = lines[i];
  		if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]){
  			return squares[a];
  		}
  	}

  	return null;
  }
  
  render() { //método para mostrar en la aplicación web
  	const winner = this.calculateWinner(this.state.squares);
  	let status;
  	if(winner){
  		status = 'Winner: ' + winner;
  	}
  	else
    	status = 'Next player: ' +  (this.state.xIsNext ? 'X' : 'O');

    return (
      <div>
        <div className="status">{status}</div>
        <div className="board-row">
          {this.renderSquare(0)}
          {this.renderSquare(1)}
          {this.renderSquare(2)}
        </div>
        <div className="board-row">
          {this.renderSquare(3)}
          {this.renderSquare(4)}
          {this.renderSquare(5)}
        </div>
        <div className="board-row">
          {this.renderSquare(6)}
          {this.renderSquare(7)}
          {this.renderSquare(8)}
        </div>
      </div>
    );
  }
}

class Game extends React.Component { //clase Game(componente) con herencia de componente react
  render() { //método para mostrar en la aplicación
    return (
      <div className="game">
        <div className="game-board">
          <Board />
        </div>
        <div className="game-info">
          <div>{/* status */}</div>
          <ol>{/* TODO */}</ol>
        </div>
      </div>
    );
  }
}

// ======================

ReactDOM.render( //método principal para mostrar la aplicación
  <Game />,
  document.getElementById('root')
);
